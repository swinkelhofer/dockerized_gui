# Dockerized GUI applications

## Setup

```
git clone https://gitlab.com/swinkelhofer/dockerized_gui.git
cd dockerized_gui
./install
``` 

## PSPP

Statistic software. Run: pspp

## TorBrowser

Browse via OnionRouting (Tor Network). Run: tor
