#!/bin/bash

addgroup --gid $UID $USER
adduser --uid $UID --home $HOME --shelll /bin/bash --gid $UID --quiet --system --disabled-password  $USER

cd /home/$USER
chown -R $USER:$USER /opt/tor-browser_en-US
sudo -u $USER /opt/tor-browser_en-US/Browser/start-tor-browser
