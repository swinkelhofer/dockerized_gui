#!/bin/bash

addgroup --gid $UID $USER
adduser --uid $UID --home $HOME --shell /bin/bash --gid $UID --quiet --system --disabled-password  $USER

cd /home/$USER
sudo -u $USER /usr/bin/thunar "$@"
