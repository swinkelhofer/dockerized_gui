#!/bin/bash
C=''
for i in "$@"; do 
    i="${i//\\/\\\\}"
    C="$C \"${i//\"/\\\"}\""
done
echo "CMD:"
echo "$C"
echo 
addgroup --gid $UID $USER
adduser --uid $UID --home $HOME --gid $UID --quiet --system --disabled-password  $USER

cd /home/$USER
/usr/bin/kvm "$@"
